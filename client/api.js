const firebase = require('firebase');
const googleStorage = require('@google-cloud/storage');
var serviceAccount = require("./firebase.json");
const mime = require('mime');
const storage = googleStorage({
  projectId: "website-50d3a",
  keyFilename: "firebase.json"
})

var config = {
   apiKey: "AIzaSyAMALj2-7iA3bIEICAgU03s0kkW8YlQeFA",
   authDomain: "website-50d3a.firebaseapp.com",
   databaseURL: "https://website-50d3a.firebaseio.com/",
 };
 firebase.initializeApp(config);

 // Get a reference to the database service
 var database = firebase.database();

const bucket = storage.bucket("gs://website-50d3a.appspot.com");

  module.exports = {
     uploadFile: function(path){
       const fileMime = mime.getType(path);
       const uploadTo = "/"+path;

       return(new Promise( function(resolve, reject) {
         bucket.upload(path,{
             destination:uploadTo,
             metadata: {contentType: fileMime,cacheControl: "public, max-age=300"}
         }, function(err, file) {
                 resolve()
             if(err){
                 reject()
             }
         })
       })
       )
     },
     downLoadFile: function (path){
       return new Promise(function(resolve, reject){
         const options = {
           destination: "./app/"+path,
         };
         bucket.file(path).download(options).then(() => {
         resolve()
       }).catch(err => {
         reject()
       })
       })
     },
     getValue: function(func){
       var ref = firebase.database().ref('bridge/reload');
       ref.on('value', function(snapshot) {
         func(snapshot.val())
       });
     },
     writeValue: function(val){
       firebase.database().ref('bridge/').set({
        reload:val
      });
     }
  }
