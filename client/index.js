const { exec } = require('child_process');
const { spawn } = require('child_process');
const fs = require('fs')
const api = require("./api")
const files = require("./files")
let proc;
const start_process = ()=> {
 fs.chmodSync('./app/run.sh', 0777)
  return spawn('./app/run.sh', {detached: true});;
}
const kill_process = child =>{
  process.kill(-child.pid);
}

  api.downLoadFile("run.zip").then(()=>{
    files.unzip("./app/run.zip").then(()=>{
      proc = start_process()
    })
  })

const handleChange = value=>{
  if(value){
    if(proc){
      try{
        kill_process(proc)
      }catch(e){
        console.log("cannot kill")
      }
    }
    api.writeValue(false)
      api.downLoadFile("run.zip",).then(()=>{
        files.unzip("./app/run.zip").then(()=>{
          proc = start_process()
        })
      })
  }
}
api.getValue(function(value){
  handleChange(value)
})
