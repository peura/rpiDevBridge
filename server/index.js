const fs = require('fs')
const api = require("./api")
const files = require("./files")
api.writeValue(false)
const params = []
process.argv.forEach(function (val, index, array) {
  if(index!=0&&index!=1){
    params.push(val)
  }
});
const path = params[0]
files.makeZip(path).then(()=>{
  api.uploadFile("run.zip").then(()=>{
    api.writeValue("true").then(()=>{
      console.log("succes")
      process.exit()
    })
  })
})
