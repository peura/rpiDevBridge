var fs = require("fs");
var zipdir = require('zip-dir');
var fs = require('fs');
const unzipper = require("unzipper")

module.exports = {
     makeZip: function(path){
       return new Promise(function(resolve, reject){
         zipdir(path, { saveTo: './run.zip' }, function (err, buffer) {
           resolve()
         });
       })

     },
     unzip: function(path){
       return new Promise(function(resolve, reject){
         var extract = require('extract-zip')
         extract(path, {dir: process.env.PWD+"/app"}, function (err) {
           if(err){
             console.log(err)
           }
           resolve()
         })
       })
     }
  }
